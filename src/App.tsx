import React, { useState } from 'react';
import appStyle from './App.module.css';
import { Contact } from './services/contacts/contact';
import { getContacts } from './services/contacts/contactsService';
import { ContactsRequest } from './services/contacts/contactsRequest';
import { error } from 'console';
import { Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Redirect from './pages/Redirect';

function App() {

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/accesstoken/callback" element={<Redirect />} />
    </Routes>
  );
}

export default App;

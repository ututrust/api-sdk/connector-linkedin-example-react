import { Contact } from "./contact";

export class ContactsResponse {

  constructor(
    public contacts: Contact[]) { }
}
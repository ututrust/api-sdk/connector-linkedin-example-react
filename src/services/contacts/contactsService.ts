import { createUnsecuredAxiosInstance } from "../../helpers/axios/axiosHelper";
import { ContactsResponse } from "./contactsReponse";
import { ContactsRequest } from "./contactsRequest";

export const getContacts = async (contactsRequest: ContactsRequest) => {
  let url = '/contacts/get';

  let config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  let { data } =
    await createUnsecuredAxiosInstance().post<ContactsResponse>(
      url, contactsRequest, config);

  return data;
}
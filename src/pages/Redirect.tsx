import { useEffect, useState } from "react";
import { Contact } from "../services/contacts/contact";
import { ContactsRequest } from "../services/contacts/contactsRequest";
import { getContacts } from "../services/contacts/contactsService";
import { useLocation } from "react-router-dom";
import appStyle from '../App.module.css';



export default function Redirect() {
  const [contacts, setContacts] = useState<Contact[]>([]);
  const location = useLocation();

  useEffect(() => {
    const fetchContacts = async (accessCode: string) => {
      let contactRequest = new ContactsRequest(accessCode);
      let contactResponse = await getContacts(contactRequest);
      setContacts(contactResponse.contacts);
    }

    const queryParams = new URLSearchParams(location.search);
    const accessCode = queryParams.get('accessCode');

    if (accessCode) {
      fetchContacts(accessCode);
    }

  }, []);

  const getContactsJSX = () => {
    if (contacts.length > 0) {
      return (
        <div style={{ marginTop: '10px' }}>
          <div className={appStyle.row}>
            <div className={appStyle.col_33}>
              <span className={appStyle.columnHeader}>First Name</span>
            </div>
            <div className={appStyle.col_33}>
              <span className={appStyle.columnHeader}>Last Name</span>
            </div>
            <div className={appStyle.col_34}>
              <span className={appStyle.columnHeader}>Linked In URL</span>
            </div>
          </div>
          {contacts?.map(
            (contact, i) =>
              <div className={appStyle.row} key={i}>
                <div className={appStyle.col_33}>
                  <label>{contact.firstName}</label>
                </div>
                <div className={appStyle.col_33}>
                  <label>{contact.lastName}</label>
                </div>
                <div className={appStyle.col_34}>
                  <label>{contact.linkedInHandle}</label>
                </div>
              </div>
          )
          }
        </div>
      )
    } else {
      return ('No Connections found');
    }
  }

  return (
    <>
      {getContactsJSX()}
    </>
  );
}

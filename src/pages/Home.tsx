import { useState } from 'react';
import appStyle from '../App.module.css';
import { Contact } from '../services/contacts/contact';


export default function Home() {

  const openLinkedInLoginForm = () => {

    let linkedInClientId = process.env.LINKEDIN_CLIENT_ID;
    let linkedInCallbackUrl = process.env.LINKEDIN_CALLBACK_URL;

    console.log('linkedInClientId', linkedInClientId);
    console.log('linkedInCallbackUrl', linkedInCallbackUrl);

    // const oauthUrl =
    //   'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=' +
    //   linkedInClientId + '&scope=profile,email&state=123456&redirect_uri=' +
    //   linkedInCallbackUrl;

    // const width = 450;
    // const height = 730;
    // const left = window.screen.width / 2 - width / 2;
    // const top = window.screen.height / 2 - height / 2;

    // window.open(
    //   oauthUrl,
    //   "Linkedin",
    //   "menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=" +
    //   width +
    //   ", height=" +
    //   height +
    //   ", top=" +
    //   top +
    //   ", left=" +
    //   left
    // );
  };

  return (
    <div className={appStyle.container}>
      <div className={appStyle.title}>Get Contacts of Linkedin User</div>
      <div className={appStyle.row}>
        <div className={appStyle.col_75}>
          Login to Linkedin to fetch connections
        </div>
        <div className={appStyle.col_25}>
          <button className={appStyle.button}
            onClick={openLinkedInLoginForm}>Login</button>
        </div>
      </div>
    </div>
  );
}


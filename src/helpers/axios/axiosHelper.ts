import axios, { AxiosInstance } from 'axios';

axios.defaults.baseURL = 'http://localhost:8080';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.timeout = 5000;

export const createUnsecuredAxiosInstance = () => {
  let unSecuredAxiosInstance: AxiosInstance = axios.create({});
  return unSecuredAxiosInstance;
};
